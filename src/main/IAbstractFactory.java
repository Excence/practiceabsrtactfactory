package main;

public interface IAbstractFactory<T extends Car>  {
    T create();
}
