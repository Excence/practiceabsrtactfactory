package main;

public class AbstractFactory<T> implements IAbstractFactory {
    @Override
    public T create() {
        return new T();
    }
}
